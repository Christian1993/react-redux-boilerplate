import React, { Component } from "react";

export default class About extends Component {
  render() {
    return (
      <main className="main">
        <section className="about">
          <div className="container">
            <div className="row">
              <h2>About page</h2>
            </div>
          </div>
        </section>
      </main>
    );
  }
}
