import React, { Component } from "react";

import Users from "../Users";

export default class Home extends Component {
  render() {
    return (
      <main className="main">
        <section className="intro">
          <div className="container">
            <div className="row">
                <Users />            
            </div>
          </div>
        </section>
      </main>
    );
  }
}
