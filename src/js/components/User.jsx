import React, { Component } from "react";

export default class User extends Component {
  render() {
    const { name, username, email } = this.props.user;
    return (
      <div className="user">
        <div className="user__name">Name: {name}</div>
        <div className="user__username"> Username:{username}</div>
        <div className="user__email">Email: {email}</div>
      </div>
    );
  }
}
