import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Navbar extends Component {
  render() {
    return (
      <nav className="navbar">
        <ul className="navbar__list">
          <li>
            <NavLink exact to="/" className="navbar__link">
              Homepage
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" className="navbar__link">
              About
            </NavLink>
          </li>
          <li>
            <NavLink to="/blog" className="navbar__link">
              Blog
            </NavLink>
          </li>
        </ul>
      </nav>
    );
  }
}
