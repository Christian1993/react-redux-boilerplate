import React from "react";
import Navbar from "./Navbar";

const Header = props => {
  return (
    <header className="header">
      <Navbar />
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <h1>{props.title}</h1>
          </div>

        </div>
      </div>
    </header>
  );
};

export default Header;
