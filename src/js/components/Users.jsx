import React, { Component } from "react";
import { connect } from "react-redux";

import { getUsers } from "../actions/userActions";
import User from "./User";

class Users extends Component {
  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    return (
      <>
        {this.props.users.map(user => (
          <div key={user.id} className="col-sm-6">
            <User user={user} />
          </div>
        ))}
      </>
    );
  }
}

const mapStateToProps = state => ({
  users: state.user.users
});

export default connect(
  mapStateToProps,
  { getUsers }
)(Users);
