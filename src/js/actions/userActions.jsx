import { GET_USERS } from "./types";
import axios from "axios";

export const getUsers = () => dispatch => {
  axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then(response => dispatch({ type: GET_USERS, payload: response.data }))
    .catch(() => alert("Something gone wrong, please try later..."));
};
